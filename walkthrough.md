# HTML, CSS, SVG Walkthrough
## Preparation
- Create index.html
- Start live server (see extensions)
- Open console

Aim: building up towards simple plots using HTML, CSS and SVG

## HTML server
Opening `html` file in browser (`file://long_path/index.html`) is not the same as serving the file (`http://localhost:5500/index.html`). => need to have live server in vscode

## What is HTML?
Markup language for the web.

```html
Cool topic
This is the description of that topic. It's really interesting.
```

Using markup:
```html
<h1>Cool topic</h1>
This is the description of that topic. It's really interesting.
```

## Structure of HTML documents
```html
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
    </body>
</html>
```

`head` is for:
- setting the title of your page (what is shown on the tab)
- loading stylesheets
- loading javascript files

`body` is for the actual content

## HTML tags
Different types of tags:
- `<!-- ... -->`: comment
- `<a>`: hyperlink
- `<div>`: generic section
- `<p>`: new paragraph
- `<h1>`, ..., `<h6>`: heading of level 1 to 6
- `<em>`: emphasized text
- `<link>`: defines a relationship to an external file
- ...

Try out:
- `title`
- `p`
- `h1`
- `a`

<!-- General HTML file structure -->
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>
        Cool topic
        This is the description of that topic. It's really interesting.        
    </body>
</html>